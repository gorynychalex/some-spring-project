package ru.popovich.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SomeSpringProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SomeSpringProjectApplication.class, args);
	}

}
